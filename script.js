$(document).ready (function() 
{
	$('ul.tabs').each(function()//Shows related content for each tab 
	{
				
		var $active, $content, $links = $(this).find('a');
				
		$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
		$active.addClass('active');


		$content = $($active[0].hash);

				

		$links.not($active).each(function ()//Hides content not selected 
		{
			$(this.hash).hide();
		});		

		$(this).on('click', 'a', function(e)//When new tab is clicked show related content
		{

				
		if($(this).parent('li').hasClass("ddl"))//If tab has a dropdown list avoid showing class
		{	
		}
				
		else
		{
			$active.removeClass('active');
					
			$content.hide();
				
			$active = $(this);	

			$content = $(this.hash);

			$active.addClass('active');

			$content.show();									
		}	
			e.preventDefault();				
		});

	});
		
	$('.ddl').hover(function()//When a button with a dropdown list is hovered over toggle the related sub menu's
	{
		$(this).children('.sub').slideToggle(50);
	});		
});
		
$(document).ready(function() 
{
	clickImage($('#games'));
	clickImage($('#books'));
	
	function clickImage($id)//Code for when an image is clicked
	{
	
		$id.each(function()//Sorts each ID and hides content not to be shown 
		{
			var $active, $content, $links = $(this).find('a');	
				
			$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);						

			$content = $($active[0].hash);
						
			$content.hide();
						
			$links.not($active).each(function () 
			{
				$(this.hash).hide();				
			});		

			$(this).on('click', 'a', function(e)//When image is clicked fades in and out the related text
			{
					if($(this).hasClass('active'))//If related text is already selected makes text disappear when image is clicked again
					{
						$active.removeClass('active');
						$content.fadeOut();
					}

					else
					{
						$content.fadeOut().finish();
									
						$active.removeClass('active');					
									
						$active = $(this);	

						$content = $(this.hash);

						$active.addClass('active');								
									
						$content.fadeIn();									
					}
						e.preventDefault();	
					});	
		});
	}
});

$(document).ready(function() 
{
	$('#characters .characterNames').click(function()//Code to do accordion buttons
	{
		var current = $(this).closest('.characterNames').find('.accordionButton');
		var content = $(this).closest('.characterNames').find('.characterContent');
		$('#characters .accordionButton').not(current).animate({marginLeft:"0%"});//Animates text in accordion button back to starting point if different accordion is clicked
		$('#characters .characterContent').not(content).slideUp();
		content.slideDown();
		current.animate({marginLeft:"70%"});//Animates text to right when accordion button is clicked		
	});
});

$(document).ready(function() 
{
	// Tooltip text
	$('.masterTooltip').hover(function()
	{
		// Hover over code
		var title = $(this).attr('title');
		$(this).data('tipText', title).removeAttr('title');
		$('<p class="tooltip"></p>')
		.text(title)
		.appendTo('body')
		.fadeIn('slow');
	}, function() 
	{
		// Hover out code
		$(this).attr('title', $(this).data('tipText'));
		$('.tooltip').remove();
	}).mousemove(function(e) 
	{
		var mousex = e.pageX + 20; //Get X coordinates
		var mousey = e.pageY + 10; //Get Y coordinates
		$('.tooltip')
		.css({ top: mousey, left: mousex })
	});
});

$(document).ready(function() 
{
	$('#add_title').click(function(evt)//When add title button is clicked a table row will be added to the table with the information in the text box
	{
		var numberText = $("tr:last > td:first").text();//Selects last table row then the first table column and puts it in to variable
		var number = parseInt(numberText) + 1;//Makes text in to a number and assigns it to a variable
		$("#title>tr.current").removeClass("current");
		$("#title").append($('<tr class="current"><td>' +number+ '</td><td>' + $('#titleText').val() +'</td><td></td></tr>'));
	});
});

$(document).ready(function()
{
    $("select").change(function()
	{
        var color = $("#colour").val();
		$("html").css("background",color);
    });
});

